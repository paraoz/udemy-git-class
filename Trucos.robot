*** Settings ***

Library         SeleniumLibrary
Resource        Recursos.robot

*** Test Cases ***

001- Ir al Blog de Winston
    AbrirNavegador
    Title Should Be     Hola Mundo!
    Set Focus To Element       xpath=/html/body/div[1]/div/div[2]/a[1]
    Click Link          xpath=/html/body/div[1]/div/div[2]/a[1]
    Wait Until Element Is Visible                    xpath=//*[@id="page-header"]/div[1]/div/div/div/a
    Title Should Be     Winston Castillo – Un sitio para comunicarse
    Close Browser



002- Abrir Ventana Modal
    [Tags]   Error
    AbrirNavegador
    Title Should Be     Hola Mundo!
    Click Link          xpath=//*[@id="Menu "]/li[1]/a
    Set Focus To Element       xpath=/html/body/div[1]/div/div[2]/a[2]
    Click Link          xpath=/html/body/div[1]/div/div[2]/a[2]
    Wait Until Element Is Visible                    xpath=//*[@id="exampleModalLabel"]
    Title Should Be     Hola Mundo!
    Close Browser


